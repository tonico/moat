<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::get();

        if($users->count() == 0) {
            User::create(array(
                'email' => 'admin@test.com',
                'password' => Hash::make('admin'),
                'name'  => 'Admin Moat',
                'role'  => '1'
            ));

            User::create(array(
                'email' => 'user@test.com',
                'password' => Hash::make('user'),
                'name'  => 'User Moat',
                'role'  => '2'
            ));
        }
    }
}
