<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlbumRequest;
use App\Models\Album;
use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlbumController extends Controller
{
    public function index()
    {
        $albuns = Album::_index();

        return view('album.index')
            ->with('registers', $albuns)
            ;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $artists = DB::table('artists')->orderBy('name')->pluck('name', 'id')->prepend(trans('messages.select_artist'), '');;
        return view('album.create')
            ->with('artists', $artists)
            ;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(AlbumRequest $request)
    {
        $input = $request->all();

        Album::create($input);

        \Session::flash('message', trans('messages.conf_inc_album'));
        $url = $request->get('redirect_to', asset('album'));
        return redirect()->to($url);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $album = Album::find($id);
        $artists = DB::table('artists')->orderBy('name')->pluck('name', 'id')->prepend(trans('messages.select_artist'), '');

        return view('album.show')
            ->with('album', $album)
            ->with('artists', $artists)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $album = Album::find($id);
        $artists = DB::table('artists')->orderBy('name')->pluck('name', 'id')->prepend(trans('messages.select_artist'), '');

        return view('album.edit')
            ->with('album', $album)
            ->with('artists', $artists)
            ;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(AlbumRequest $request, $id)
    {
        $reg = Album::find($id);
        $reg->update($request->all());

        \Session::flash('message', trans('messages.conf_upd_album'));
        $url = $request->get('redirect_to', asset('album'));
        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        // testa se tem cidades ligadas a este país
        $i = DB::select('select count(*) as qtd from albuns where artist_id = ' . $id);
        if ($i[0]->qtd > 0) {
            \Session::flash('error', trans('messages.erro_del_artist'));
            return redirect()->back();
        }

        // apaga o registro
        Artist::find($id)->delete();


        \Session::flash('message', trans( 'messages.conf_del_artist'));
        return redirect()->to(asset('artist'));
    }

}
