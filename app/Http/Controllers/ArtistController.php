<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArtistRequest;
use App\Http\Resources\ArtistResource;
use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtistController extends Controller
{
    public function index()
    {
        $artists = Artist::orderBy('name')->get();

        return view('artist.index')
            ->with('registers', $artists)
            ;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('artist.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ArtistRequest $request)
    {
        $input = $request->all();

        Artist::create($input);

        \Session::flash('message', trans('messages.conf_inc_artist'));
        $url = $request->get('redirect_to', asset('artist'));
        return redirect()->to($url);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $artist = Artist::find($id);

        return view('artist.show')
            ->with('artist', $artist)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $artist = Artist::find($id);

        return view('artist.edit')
            ->with('artist', $artist)
            ;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ArtistRequest $request, $id)
    {
        $reg = Artist::find($id);
        $reg->update($request->all());

        \Session::flash('message', trans('messages.conf_upd_artist'));
        $url = $request->get('redirect_to', asset('artist'));
        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        // testa se tem cidades ligadas a este país
        $i = DB::select('select count(*) as qtd from albuns where artist_id = ' . $id);
        if ($i[0]->qtd > 0) {
            \Session::flash('error', trans('messages.erro_del_artist'));
            return redirect()->back();
        }

        // apaga o registro
        Artist::find($id)->delete();


        \Session::flash('message', trans( 'messages.conf_del_artist'));
        return redirect()->to(asset('artist'));
    }

    public function index_json() {

        if (isset($_GET['artist_id'])) {
            $id = $_GET['artist_id'];
            $artists = Artist::where('id', $id)->select('id', 'name')->get()->toJson();
        }
        else {
            $artists = Artist::select('id', 'name')->orderBy('name')->get()->toJson();;
        }
        return $artists;
    }
}
