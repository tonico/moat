<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name')->get();

        return view('users.index')
            ->with('registers', $users)
            ;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
        $input = $request->all();
        $input['role'] = ($input['role'] == 'Admin')?'1':'2';

        // cria a senha padrão
        $input['password'] = Hash::make('123456');

        User::create($input);

        \Session::flash('message', trans('messages.conf_inc_user'));
        \Session::flash('message', trans('messages.conf_password_default'));
        $url = $request->get('redirect_to', asset('users'));
        return redirect()->to($url);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('users.show')
            ->with('user', $user)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('users.edit')
            ->with('user', $user)
            ;
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        $reg = User::find($id);

        $input = $request->all();
        $input['role'] = ($input['role'] == 'Admin')?'1':'2';

        $reg->update($input);

        \Session::flash('message', trans('messages.conf_upd_user'));
        $url = $request->get('redirect_to', asset('users'));
        return redirect()->to($url);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        // apaga o registro
        User::find($id)->delete();


        \Session::flash('message', trans( 'messages.conf_del_user'));
        return redirect()->to(asset('users'));
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('\\');
    }

}
