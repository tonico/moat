<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reg = $this->post('id');
        $id = $reg ? $reg : NULL;

        return [
            "name"  => "required|min:3|max:50|unique:users,name,$id,id",
            "email" => "required|min:3|max:50|unique:users,email,$id,id",
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => trans('messages.crit_name_required'),
            'name.unique'      => trans('messages.crit_name_unique'),
            'name.min'         => trans('messages.crit_name_min'),
            'name.max'         => trans('messages.crit_name_max'),

            'email.required'    => trans('messages.crit_email_required'),
            'email.unique'      => trans('messages.crit_email_unique'),
            'email.min'         => trans('messages.crit_email_min'),
            'email.max'         => trans('messages.crit_email_max'),
        ];
    }
}
