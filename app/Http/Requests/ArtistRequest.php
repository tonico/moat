<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArtistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reg = $this->post('id');
        $id = $reg ? $reg : NULL;

        return [
            "name" => "required|min:3|max:50|unique:artists,name,$id,id",
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => trans('messages.crit_name__required'),
            'name.unique'      => trans('messages.crit_name_unique'),
            'name.min'         => trans('messages.crit_name_min'),
            'name.max'         => trans('messages.crit_name_max'),
        ];
    }

}
