<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reg = $this->post('id');
        $id = $reg ? $reg : NULL;

        return [
            "name" => "required|min:3|max:50",
            "year" => "required|min:4",
            "artist_id" => "required"
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => trans('messages.crit_name_required'),
            'name.unique'       => trans('messages.crit_name_unique'),
            'name.min'          => trans('messages.crit_name_min'),
            'name.max'          => trans('messages.crit_name_max'),
            'year'              => trans("messages.crit_year_required"),
            'artist_id.required'=> trans('messages.crit_artist_required'),
        ];
    }}
