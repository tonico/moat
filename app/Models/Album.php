<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'year',
        'artist_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'Albuns';
    public $timestamps = true;

    static function _index() {
        $sql = "select a.*, b.name as artist_name ";
        $sql .= " from albuns a left join artists b on a.artist_id = b.id ";
        $sql .= " order by a.name";

        $registros = \DB::select($sql);
        return $registros;
    }
}
