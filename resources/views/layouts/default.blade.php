@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('body')

    {{-- Main Content --}}
    <div class="content">
        @yield('content')
    </div>

@stop

@section('adminlte_js')
    @stack('js')
    @yield('js')
@stop

