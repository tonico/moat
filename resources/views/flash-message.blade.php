@if ($message = Session::get('success'))
    <br/>
    <div class="alert alert-success alert-block" style="margin-bottom: 10px;margin-top: 5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br/>
@endif

@if ($message = Session::get('error'))
    <br/>
    <div class="alert alert-danger alert-block" style="margin-bottom: 10px;margin-top: 5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br/>
@endif

@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block" style="margin-bottom: 10px;margin-top: 5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('info'))
    <div class="row" style="margin-bottom: 0px;">
    <div class="alert alert-info alert-block" style="margin-bottom: 10px;margin-top: 5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    </div>
@endif

@if ($message = Session::get('message'))
    <div class="alert alert-info alert-block" style="margin-bottom: 10px;margin-top: 5px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" style="margin-bottom: 10px;margin-top: 5px;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $error }}</strong>
        </div>
    @endforeach
@endif
