@extends('adminlte::page')
@section('title', 'Moat')


@section('content')
    @include('flash-message')
    {!! Form::model($artist,[ 'route'=>['artist.update', $artist->id], 'method'=>'put', 'id'=>'form_']) !!}
    <div class="panel">
        <div class="panel-heading panel-info">
            <h4>@lang('messages.artist')</h4>
        </div>
        <div class="panel-body">
            @include ('artist._form')
        </div>

        <div class="panel-footer">
            <div class="form-group">
                {!! Form::submit(trans('messages.confirm'), ['class'=>'btn btn-success pull-left']) !!}
                <a href="{{ asset('artist') }}" class="btn btn-info pull-left">{!! trans('messages.cancel') !!}</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
