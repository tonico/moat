{!! Form::hidden('id', null, ['class'=>'form-control']) !!}

<div class="row">
    <div class="form-group col-sm-7">
        {!! Form::label('name', trans("messages.name")) !!}
        {!! Form::text('name', null, ['class'=>'form-control', 'maxlength' => '50', 'id'=>'name', 'required' => 'true']) !!}
    </div>
</div>
