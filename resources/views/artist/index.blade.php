@extends('adminlte::page')
@section('title', 'Moat'))

@section('css')
    {!!Html::style('plugins/Select/css/select.dataTables.min.css') !!}
    {!!Html::style('plugins/Buttons/css/buttons.bootstrap4.min.css')!!}
    {!!Html::style('plugins/Buttons/css/buttons.dataTables.min.css')!!}
    {!!Html::style('plugins/DataTables/css/dataTables.bootstrap4.min.css')!!}
@stop

@section('content')
    @include('flash-message')
    <div class="panel">
        <div class="panel-heading panel-info">
            <h4>@lang('messages.artist')</h4>
        </div>
        <div class="panel-body">
            <table class="table" id="tbl_">
                <thead>
                    <th>#</th>
                    <th>@lang('messages.name')</th>
                </thead>
                <tbody>
                @foreach($registers as $reg)
                    <tr>
                        {!! Form::open(['route' => [ 'artist.destroy' ,'artist' => $reg->id], 'method' =>'DELETE', 'id' => "delete-form-{$reg->id}", 'style' => 'display:none' ]) !!}
                        {!! Form::close() !!}
                        <td>{{$reg->id}}</td>
                        <td>{{$reg->name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    {!!Html::script("plugins/DataTables/js/jquery.dataTables.min.js")!!}
    {!!Html::script("plugins/DataTables/js/dataTables.bootstrap4.min.js")!!}
    {!!Html::script("plugins/Select/js/dataTables.select.min.js")!!}
    {!!Html::script("plugins/Buttons/js/dataTables.buttons.min.js")!!}
    {!!Html::script("plugins/Buttons/js/buttons.bootstrap.min.js")!!}

    <script>
        $(document).ready(function () {
            $('#tbl_').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "order": [[ 1, 'asc' ]],
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "select": true,
                "columnDefs": [
                    {"targets": [0], "visible": false },
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        "text": "{!! trans('messages.i_insert')!!}",
                        "titleAttr": "{!! trans('messages.insert')!!}",
                        "action": function (e, dt, node, config) {
                            location.href = "{!! asset('artist/create') !!}";
                        }
                    },
                    {
                        "text": "{!! trans('messages.i_update')!!}",
                        "titleAttr": "{!! trans('messages.update')!!}",
                        "action": function (e, dt, node, config) {
                            //dados = $('#tbl_').row('.selected').data();
                            dados = $('#tbl_').DataTable().row('.selected').data();
                            if (dados == null) {
                                Swal.fire("{!! trans('messages.sSelecione') !!}")
                            }
                            else {
                                // pega o código
                                id = dados[0];
                                url = '{{ asset('artist')  }}/' + id + '/edit';
                                location.href = url;
                            }
                        }
                    },
                    {
                        "text": "{!! trans('messages.i_show')!!}",
                        "titleAttr": "{!! trans('messages.show')!!}",
                        "action": function (e, dt, node, config) {
                            dados = $('#tbl_').DataTable().row('.selected').data();
                            if (dados == null) {
                                Swal.fire("{!! trans('messages.sSelecione') !!}")
                            }
                            else {
                                // pega o código
                                id = dados[0];
                                url = '{{ asset('artist')  }}/' + id;
                                location.href = url;
                            }
                        }
                    },
                    {
                        "text": "{!! trans('messages.i_delete')!!}",
                        "titleAttr": "{!! trans('messages.delete')!!}",
                        "action": function (e, dt, node, config) {
                            dados = $('#tbl_').DataTable().row('.selected').data();
                            if (dados == null) {
                                Swal.fire("{!! trans('messages.sSelecione') !!}")
                            }
                            else {
                                // pega o código
                                var x_id = dados[0];
                                var x_name = dados[1];

                                Swal.fire({
                                    title: 'Do you want to delete the artist ' + x_name + '?',
                                    showCancelButton: true,
                                    confirmButtonText: 'Delete',
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        //Swal.fire('Saved!', '', 'success')
                                        var x_form = '#delete-form-' + x_id;
                                        $(x_form).submit();
                                    }
                                });
                            }
                        }
                    }
                ]
            });

            $('.dataTables_filter input').addClass('form-control pull-right');
            $('.dataTables_filter input').attr('placeholder', 'Search');
            $('.dataTables_filter').addClass('pull-right');
            $('.dt-buttons').addClass('pull-left');
        });
    </script>
@stop





