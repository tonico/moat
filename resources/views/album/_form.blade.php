{!! Form::hidden('id', null, ['class'=>'form-control']) !!}

<div class="row">
    <div class="form-group col-sm-7">
        {!! Form::label('name', trans("messages.album_name")) !!}
        {!! Form::text('name', null, ['class'=>'form-control', 'maxlength' => '50', 'id'=>'name', 'required' => 'true']) !!}
    </div>
    <div class="form-group col-sm-1">
        {!! Form::label('year', trans("messages.year")) !!}
        {!! Form::text('year', null, ['class'=>'form-control', 'maxlength' => '4', 'id'=>'year', 'required' => 'true']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-7">
        {!! Form::label('artist_id', trans("messages.artist")) !!}
        {!! Form::select('artist_id', $artists, null, ['class'=>'form-control', 'id'=>'artist_id', 'required' => 'true']) !!}
    </div>
</div>
