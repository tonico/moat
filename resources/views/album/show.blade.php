@extends('adminlte::page')
@section('title', 'Moat')


@section('content')
    {!! Form::model($album,[ 'route'=>['album.update', $album->id], 'method'=>'put', 'id'=>'form_', 'disabled'=>'disabled', 'role'=>'form']) !!}
    <div class="panel">
        <div class="panel-heading panel-info">
            <h4>@lang('messages.album')</h4>
        </div>

        <div class="panel-body">
            <fieldset disabled>
                @include ('album._form')
            </fieldset>
        </div>

        <div class="panel-footer">
            <div class="form-group">
                <!--{!! Form::submit(trans('messages.confirm'), ['class'=>'btn btn-success pull-left']) !!}-->
                <a href="{{ asset('artist') }}" class="btn btn-info pull-left">{!! trans('messages.back') !!}</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
