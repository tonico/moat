{!! Form::hidden('id', null, ['class'=>'form-control']) !!}

<div class="row">
    <div class="form-group col-sm-7">
        {!! Form::label('name', trans("messages.name")) !!}
        {!! Form::text('name', null, ['class'=>'form-control', 'maxlength' => '50', 'id'=>'name', 'required' => 'true']) !!}
    </div>

    <div class="form-group col-sm-3">
        {!! Form::label('name', trans("messages.role")) !!}
        @php
            $role = 1;
            if (isset($user) == true) {
                $role = $user->role;
            }

            $chk_admin = ($role==1)?'checked':'';
            $chk_user  = ($role==2)?'checked':'';
        @endphp
        <div class="row" style="height: 40px" id="radio_fuma">
            <div class="col-4 div-radio">
                <input type="radio" name="role" id="role" value="Admin" {!! $chk_admin !!}>
                {!! trans('messages.admin') !!}
            </div>
            <div class="col-4 div-radio">
                <input type="radio" name="role" id="role" value="Admin" {!! $chk_user !!}>
                {!! trans('messages.user') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-7">
        {!! Form::label('email', trans("messages.email")) !!}
        {!! Form::text('email', null, ['class'=>'form-control', 'maxlength' => '50', 'id'=>'name', 'minlength'=>'8', 'required' => 'true']) !!}
    </div>
</div>
