<?php
return [

    'admin'         => 'Admin',
    'album'         => 'Album',
    'album_name'    => 'Album Name',
    'artist'        => 'Artist',
    'back'          => 'Back',
    'cancel'        => 'Cancel',
    'confirm'       => 'Confirm',
    'delete'        => 'Delete',
    'email'         => 'E-mail',
    'insert'        => 'Insert',
    'name'          => 'Name',
    'save'          => 'Save',
    'select_artist' => 'select an artist',
    'role'          => 'Role',
    'update'        => 'Update',
    'user'          => 'User',
    'users'         => 'Users',
    'year'          => 'Year',

    // imagens da toolbar
    'i_delete'      => "<i class='fa fa-trash'></i>",
    'i_insert'      => "<i class='fa fa-plus'></i>",
    'i_update'      => "<i class='fa fa-pen'></i>",
    'i_cancel'      => "<i class='fa fa-times'></i>",
    'i_confirm'     => "<i class='fa fa-check'></i>",
    'i_search'      => "<i class='fa fa-search'></i>",
    'i_show'        => "<i class='fa fa-eye'></i>",
    'i_back'        => "<i class='fa fa-arrow-circle-left'></i>",

    // mensagens do table
    "sLengthMenu"  => "Show _MENU_ registers with page",
    "sZeroRecords" => "No record found",
    "sInfo"        => "Show _START_ / _END_ with _TOTAL_ register(s)",
    "sInfoEmpty"   => "Show 0 / 0 de 0 registers",
    "sInfoFiltered"=> "(filtered from _MAX_ registros)",
    "sSearch"      => "Search: ",
    "sFirst"       => "First",
    "sPrevious"    => "Previous",
    "sNext"        => "Next",
    "sLast"        => "Last",
    "sSelecione"   => "Select register",

    // criticas
    'crit_name_required'    => 'A Name is required.',
    'crit_name_unique'      => 'Name already registered.',
    'crit_name_min'         => 'The minimum length for the name is 3 characters.',
    'crit_name_max'         => 'The maximum length for the name is 50 characters.',

    'crit_artist_required'  => 'An artist is required',
    'crit_year_required'    => 'A Year is required',

    'crit_email_required'   => 'An e-mail is required.',
    'crit_email_unique'     => 'E-mail already registered.',
    'crit_email_min'        => 'The minimum length for the e-mail is 7 characters.',
    'crit_email_max'        => 'The maximum length for the name is 50 characters.',

    // avisos de erro
    'erro_del_artist'       => 'Artist still has an album tied to their name. Cannot delete yet.',
    'conf_del_artist'       => 'Deleted artist successfully!',
    'conf_del_user'         => 'Deleted user successfully!',

    'conf_inc_album'        => 'Album inserted successfully.',
    'conf_inc_artist'       => 'Artist inserted successfully.',
    'conf_inc_user'         => 'User inserted successfully.',
    'conf_password_default' => 'The new user password is 123456',

    'conf_upd_album'        => 'Album updated.',
    'conf_upd_artist'       => 'Artist updated.',
    'conf_upd_user'         => 'User updated.',

];
