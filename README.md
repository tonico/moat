<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Desenvolvido em Laravel

Pequena aplicação desenvolvida em Laravel / SQLite como um teste de conhecimento.

- baixe o git:
- rode: composer update
- crie o banco de dados:
- crie o login:
- para testar: login: tonicorj@gmail.com senha: 87654321
- URL para testar as APIs:

### Componentes

- **[AdminLTE](https://https://adminlte.io/themes/v3//)**
- **[Font Awesome](https://https://fontawesome.com/v4.7/icon/eye)**
- **[laravelcollective/html](https://https://github.com/LaravelCollective/html)**
- **[patricktalmadge/bootstrapper](https://https://github.com/patricktalmadge/bootstrapper)**

## Diary
- 1o. Ambient staging, installation of components, configurations;
- 2o. Migrations, route configurations, seeder, initial screen;

- 3o. Artist screen (CRUD), error messages 
- 4o. Album screen  (CRUD), error messages
- 5a. Logim and role
- 6a. APIs

# Points
- usando arquivo message para posterior tradução;
- APIs feitas com o Laravel;
- login admin@test.com / password: admin
- login user@teste.com / password: user

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
