<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function() {
    return view('home');
})->name('home')
    ->middleware('auth')
;
*/

Auth::routes();

Route::get('/'      , [App\Http\Controllers\ArtistController::class, 'index'])->middleware('auth');
Route::get('/home'  , [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('/logout', [App\Http\Controllers\UserController::class, 'logout'])->name('logout')->middleware('auth');

Route::resource('artist', 'App\Http\Controllers\ArtistController'   )->middleware('auth');
Route::resource('album' , 'App\Http\Controllers\AlbumController'    )->middleware('auth');
Route::resource('users' , 'App\Http\Controllers\UserController'     )->middleware('auth');



